package main

import (
  "encoding/json"
  "fmt"
  "flag"
  "io/ioutil"
  "os"
  "os/exec"
  "net/http"
  "path/filepath"
  "strings"
)

type Module struct {
  Name  string
  Tests []Test
}

type Test struct {
  Repository string
  Path string
  Command string
  Branch string
}

func (m Module) toString() string {
    return toJson(m)
}

func toJson(m interface{}) string {
    bytes, err := json.Marshal(m)
    if err != nil {
        fmt.Println(err.Error())
        os.Exit(1)
    }

    return string(bytes)
}

// Handle commandline parameters
var (
  url = flag.String("url", "", "URL to a HTTP remote serving the test configuration")
  target = flag.String("target", "", "The target directory to store the test cases")
  branch = flag.String("branch", "", "A branch to select tests from the configuration")
  module = flag.String("module", "", "A module to select test groups from the configuration")
)

func main() {
  flag.Parse()
  if (*url == "") || (*target == "") {
    flag.Usage()
    os.Exit(1)
  }
  // Read the configuration from the remote
  modules := getModulesFromRemote(*url)
  workModules(modules, *module)
}

func workModules(list []Module, moduleSelector string) {
  for _, m := range list {
    if moduleSelector == "" || moduleSelector == m.Name {
      fmt.Println("Work on Module: " + m.Name)
      pullTests(m.Tests, *branch)
      runTests(m.Tests, *branch)
    }
  }
}

func pullTests(list []Test, branchSelector string) {
  for _, t := range list {
    if branchSelector == "" || branchSelector == t.Branch {
      fmt.Println("Pull from " + t.Repository + " to path " + filepath.Join(*target, t.Path))
      fmt.Println("-----------------------------------------------")
      fmt.Println(createJFrogURL(t.Repository, t.Path))
      // TODO: Add your frog pull command here
      fmt.Println("===============================================")
    }
  }
}

func createJFrogURL(repository string, path string) string {
  url := filepath.Join(repository, path)
  return strings.Replace(url, "\\", "/", -1) + "/"
}

func runTests(list []Test, branchSelector string) {
  for _, t := range list {
    if branchSelector == "" || branchSelector == t.Branch {
      testCmd := t.Command
      if testCmd == "" {
        // calculate a standard test command from path
        segments := strings.Split(filepath.Clean(t.Path), string(filepath.Separator))
        testCmd = segments[len(segments)-1] + ".exe"
      }
      fmt.Println("Run " + filepath.Join(*target, t.Path, testCmd))
      fmt.Println("-----------------------------------------------")
      execCommand(filepath.Join(*target, t.Path, testCmd))
      fmt.Println("===============================================")
    }
  }
}

func execCommand(command string, params ...string) {
  cmd := exec.Command(command, params...)
  cmd.Stdout = os.Stdout
  cmd.Stderr = os.Stderr
  err := cmd.Run()
  if err != nil {
    fmt.Println(err.Error())
    os.Exit(1)
  }
}

func getModulesFromRemote(url string) []Module {
  // Read the data from remote
  resp, err := http.Get(url)
  if err != nil {
    fmt.Println(err.Error())
    os.Exit(1)
  }
  defer resp.Body.Close()
  body, err := ioutil.ReadAll(resp.Body)
  // Unmarshal the body data to our structures
  var c []Module
  json.Unmarshal(body, &c)
  return c
}
