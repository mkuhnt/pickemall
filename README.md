# Pick-em-all
Simple tool to load a test configuration from a remote URL, parse it, and
trigger pull executions based on a branch and module selector.

For Demonstration purpose you can file a compatible sample file here:
- https://s3.eu-central-1.amazonaws.com/itr-download-test/test-with-command.json
